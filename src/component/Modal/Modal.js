import React, { Component } from 'react';
import './Modal.scss';

class Modal extends Component {
  handleClose = () => {
    if (this.props.onClose) {
      this.props.onClose();
    }
  };

  render() {
    const { header, closeButton, text, actions } = this.props;

    return (
      <div className="custom-modal-overlay" onClick={this.handleClose}>
        <div className="custom-modal" onClick={(e) => e.stopPropagation()}>
          {closeButton && (
            <span className="close-button" onClick={this.handleClose}>
              &times;
            </span>
          )}
          {header && <h2>{header}</h2>}
          <p>{text}</p>
          <div className="actions">
          {actions || (
              <>
                <button className='btn-close' onClick={this.handleClose}>CLOSE</button>
                <button className='btn-ok' onClick={this.handleClose}>OK</button>
              </> )}
            </div>
        </div>
      </div>
    );
  }
}

export default Modal;
