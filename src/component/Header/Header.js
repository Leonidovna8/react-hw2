import React from "react";
import "./Header.scss"

class Header extends React.Component {
    render() {
        const { cartCount, favoritesCount } = this.props;
        return (
        <header>
            <div className="container">
            <div className="logo">Dyson</div>
            <div className="content">
        <div className="cart">
            <span role="img" aria-label="Cart">🛒</span> Cart: {cartCount}
        </div>
        <div className="favorites">
            <span role="img" aria-label="Favorites">⭐</span> Favorites: {favoritesCount}
        </div>
        </div>
        </div>
    </header>
        )
    }
}

export default Header