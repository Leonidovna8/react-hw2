import React, { Component } from 'react';
import Product from '../Product/Product.js';

class AllProducts extends Component {
    //начальное состояние
    constructor(props) {
        super(props);
        this.state = {
            //хранение инфо про продукты
            products: [],
            //отслеживание количества товаров в корзине
            cartCount: 0,
            //хранение информации о том, какие продукты добавлены в избранное
            favorites: [],
            //подсчет количества избранных товаров
            favoritesCount: 0,
        };
    }

    //загрузка данных о продуктах из файла products.json и установки их в состояние products
    componentDidMount() {
        // Загрузка даних з JSON
        fetch('/products.json')
            .then(response => response.json())
            .then(data => this.setState({ products: data }))
            .catch(error => console.error(error));
    }

    //увеличиваем счетчик товаров в корзине cartCount на 1 при добавлении продукта в корзину.
    handleAddToCart = product => {
        this.setState(prevState => ({
            cartCount: prevState.cartCount + 1,
        }));
    };

    //обрабатываем добавление продукта в избранное. Инвертируем состояние избранного продукта (true становится false и наоборот) и обновляем состояние favorites. 
    handleAddToFavorite = product => {
        this.setState(prevState => {
            const favorites = [...prevState.favorites];
            favorites.push(product)

            return favorites;
        }
        );
    };
    handleAddToFav = product => {
        this.setState(prevState => ({
            favoritesCount: prevState.favoritesCount + 1,
        }));
    };
    // Метод для подсчета количества избранных товаров на основе состояния favorites. фильтруем значения в объекте favorites и считаем количество true значений, затем обновляем состояние favoritesCount
    calculateFavoritesCount = () => {
        const { favorites } = this.state;

        const favoritesCount = Object.values(favorites).filter(value => value).length;

        this.setState({ favoritesCount });
    };

    // обновляем состояния isFavorite для всех продуктов в массиве products на основе состояния favoritesCount. Создаем новый массив updatedProducts, в котором каждый продукт имеет свойство isFavorite, определяемое наличием соответствующей записи в favoritesCount.

    updateProductFavorites = () => {
        const { products, favoritesCount } = this.state;
        const updatedProducts = products.map(product => ({
            ...product,
            isFavorite: favoritesCount[product.id] || false,
        }));
        this.setState({ products: updatedProducts });
    };

    render() {
        const { products, favorites } = this.state;

        return (
            <div className="home-page">
                <main>
                    {products.map(product => (
                        <Product
                            addToCart={this.props.addToCart}
                            onAddToFavorite={this.handleAddToFavorite}
                            key={product.id}
                            product={{
                                ...product,
                                isFavorite: favorites[product.id] || false,

                            }}
                            el={product}
                            onAddToCart={this.handleAddToCart}
                            addToFavorites={this.props.addToFavorites}
                        />
                    ))}
                </main>
            </div>
        );
    }
}

export default AllProducts;

