import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Modal from '../Modal/Modal.js';
import "./Product.scss"

class Product extends Component {
    constructor(props) {
        super(props);
        // Инициализируем состояние из пропсов
        this.state = {
            showModal: false,
            isFavorite: this.props.product.isFavorite || false, //отслеживаем добавлень ли продукт в избранное
        };
    }


    //метод, который вызывается при нажатии кнопки "Add to Cart"
    handleAddToCart = () => {
        this.setState({ showModal: true }); //устанавливаем состояние showModal в true, чтобы отобразить модальное окно
        this.props.onAddToCart(this.props.product.id); //вызываем функцию onAddToCart, передавая ей id продукта.
    };


    //метод, который переключает состояние isFavorite при нажатии кнопки "Add to Favorites"
    toggleFavorite = () => {
        const { product } = this.props;
        this.setState((prevState) => ({
            isFavorite: !prevState.isFavorite, product, // Инвертируем текущее состояние
        }));
        // this.props.onAddToFavorite(this.props.product.id, !this.state.isFavorite); //вызываем функцию onAddToFavorites, передавая id продукта и новое состояние isFavorite.

    };

    render() {
        const { product } = this.props;
        const { showModal } = this.state;

        return (
            <div className="product-card">
                <img className='img' src={product.url} alt={product.name} />
                <h3>{product.name}</h3>
                <p>Price: ${product.price.toFixed(2)} </p>
                <button className='btn-cart' onClick={() => {
                    this.props.addToCart(this.props.el);
                    this.setState({ showModal: true });
                }}>
                    Add to Cart
                </button>
                {/* при нажатии вызываем функцию  this.props.addToCart(this.props.el), которая, добавляет этот продукт в корзину. */}
                <button className='btn-favorite' onClick={() => {
                    this.props.addToFavorites(this.props.el)
                    this.props.onAddToFavorite(this.props.el); // Обновить состояние favorites
                    this.toggleFavorite();
                    // Обновить isFavorite напрямую
                    this.setState(prevState => ({
                        isFavorite: !prevState.isFavorite
                    }));
                }}>
                    {product.isFavorite ? '★' : '☆'}
                </button>
                {/* функция this.toggleFavorite, которая переключает состояние isFavorite. */}
                {showModal && (
                    <Modal
                        header="Added"
                        text={`${product.name} has been added.`}
                        onClose={() => this.setState({ showModal: false })}

                    />
                )}
            </div>
        );
    }
}

// Product.propTypes = {
//    product: PropTypes.shape({
//       id: PropTypes.number.isRequired,
//       name: PropTypes.string.isRequired,
//       price: PropTypes.number.isRequired,
//       url: PropTypes.string.isRequired,
//       color: PropTypes.string.isRequired,
//       isFavorite: PropTypes.bool
//    }).isRequired,
//    onAddToCart: PropTypes.func.isRequired,
//    onAddToFavorite: PropTypes.func.isRequired
// };

export default Product;
