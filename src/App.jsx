import React from "react";
import AllProducts from "./component/AllProducts/AllProducts.js";
import Header from "./component/Header/Header.js";
import "./App.css";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstModalOpen: false,
      secondModalOpen: false,
      cart: JSON.parse(localStorage.getItem("cart")) || [],
      favorite: JSON.parse(localStorage.getItem("favorites")) || [],
    };
  }

  // Загрузка даних з localStorage
  addToCart = (product) => {
    const cart = JSON.parse(localStorage.getItem("cart"));
    console.log(cart);
    if (cart) {
      localStorage.setItem("cart", JSON.stringify([...cart, product]));
      this.setState({ cart: JSON.parse(localStorage.getItem("cart")) });
    } else {
      localStorage.setItem("cart", JSON.stringify([product]));
      this.setState({ cart: JSON.parse(localStorage.getItem("cart")) });
    }
  };
  // Функция для добавления продукта в избранное
  addToFavorite = (product) => {
    const favorites = JSON.parse(localStorage.getItem("favorites")); // Получаем текущий список избранного из localStorage или создаем пустой массив
    if (favorites && favorites.length > 0) {
      favorites.forEach((prod) => {
        if (prod.id === product.id) {
          console.log(true);
          const newFavorites = favorites.filter((el) => el.id !== product.id);
          // delete favorites[prod]
          console.log(newFavorites);
          localStorage.setItem("favorites", JSON.stringify([...newFavorites]));
          this.setState({
            favorite: JSON.parse(localStorage.getItem("favorites")),
          });
        } else {
          localStorage.setItem(
            "favorites",
            JSON.stringify([...favorites, product])
          );
          this.setState({
            favorite: JSON.parse(localStorage.getItem("favorites")),
          });
        }
      });
    } else {
      localStorage.setItem("favorites", JSON.stringify([product]));
      this.setState({
        favorite: JSON.parse(localStorage.getItem("favorites")),
      });
    }

  }

  render() {
    return (
      <div className="App">
        <Header
          cartCount={this.state.cart.length}
          favoritesCount={this.state.favorite.length}
        />
        <AllProducts
          addToCart={this.addToCart}
          addToFavorites={this.addToFavorite}
        />
      </div>
    );
  }
}

export default App;
